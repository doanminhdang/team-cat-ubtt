<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node COLOR="#000000" CREATED="1517228844523" ID="ID_1374025020" MODIFIED="1522771223689" TEXT="Quy tr&#xec;nh d&#x1ecb;ch s&#xe1;ch v&#x1edb;i CAT cho UBTT">
<edge COLOR="#000000" WIDTH="8"/>
<font NAME="SansSerif" SIZE="20"/>
<node COLOR="#0033ff" CREATED="1517228880828" ID="ID_942331770" MODIFIED="1522771223690" POSITION="right" TEXT="&#x110;&#x1ed1;i t&#x1b0;&#x1ee3;ng">
<edge COLOR="#0033ff" WIDTH="4"/>
<node COLOR="#00b439" CREATED="1517228913591" ID="ID_387339119" MODIFIED="1522771223691" TEXT="Ng&#x1b0;&#x1edd;i d&#x1ecb;ch b&#x1eb1;ng tay">
<edge COLOR="#00b439" WIDTH="2"/>
<arrowlink DESTINATION="ID_582414427" ENDARROW="Default" ENDINCLINATION="67;0;" ID="Arrow_ID_1433723454" STARTARROW="None" STARTINCLINATION="67;0;"/>
<icon BUILTIN="full-2"/>
</node>
<node COLOR="#00b439" CREATED="1517228917432" ID="ID_212991424" MODIFIED="1522771223691" TEXT="Ng&#x1b0;&#x1edd;i l&#xe0;m vi&#x1ec7;c tr&#xea;n PC v&#x1edb;i ph&#x1ea7;n m&#x1ec1;m CAT do nh&#xf3;m ch&#x1ecd;n d&#xf9;ng chung">
<edge COLOR="#00b439" WIDTH="2"/>
<arrowlink DESTINATION="ID_582414427" ENDARROW="Default" ENDINCLINATION="76;0;" ID="Arrow_ID_1860720065" STARTARROW="None" STARTINCLINATION="76;0;"/>
<arrowlink COLOR="#b0b0b0" DESTINATION="ID_144930818" ENDARROW="Default" ENDINCLINATION="772;0;" ID="Arrow_ID_1029679696" STARTARROW="None" STARTINCLINATION="772;0;"/>
<icon BUILTIN="full-1"/>
</node>
<node COLOR="#00b439" CREATED="1517228942446" ID="ID_1614266794" MODIFIED="1522771223692" TEXT="Ng&#x1b0;&#x1edd;i d&#x1ecb;ch online ho&#x1eb7;c d&#xf9;ng CAT ri&#xea;ng">
<edge COLOR="#00b439" WIDTH="2"/>
<icon BUILTIN="full-3"/>
<node COLOR="#990000" CREATED="1517232506149" ID="ID_1249468762" MODIFIED="1522771223692" TEXT="Thay OmegaT b&#x1eb1;ng platform d&#x1ecb;ch online, nh&#x1ead;n file Bilingual DOCX v&#xe0; tr&#x1ea3; ra file &#x111;&#xe3; d&#x1ecb;ch">
<edge COLOR="#990000" WIDTH="1"/>
</node>
<node COLOR="#990000" CREATED="1517232790189" ID="ID_1777390663" MODIFIED="1522771223693" TEXT="V&#xed; d&#x1ee5;: &#x111;&#x1b0;a Bilingual DOCX v&#xe0;o Google Translator Toolkit, Smartcat hay Matecat">
<edge COLOR="#990000" WIDTH="1"/>
</node>
<node COLOR="#990000" CREATED="1517232848029" ID="ID_508997231" MODIFIED="1522771223693" TEXT="Kh&#xf3; t&#x1ed5; ch&#x1ee9;c trao &#x111;&#x1ed5;i d&#x1eef; li&#x1ec7;u t&#x1ee9;c th&#x1edd;i gi&#x1eef;a nh&#xf3;m &#x111;ang d&#x1ecb;ch tr&#xea;n OmegaT v&#x1edb;i ng&#x1b0;&#x1edd;i d&#x1ecb;ch online">
<edge COLOR="#990000" WIDTH="1"/>
</node>
<node COLOR="#990000" CREATED="1517232883609" ID="ID_925341668" MODIFIED="1522771223698" TEXT="Khi d&#xf9;ng online ho&#x1eb7;c ph&#x1ea7;n m&#x1ec1;m CAT kh&#xe1;c team OmegaT, th&#xec; t&#x1ef1; qu&#x1ea3;n l&#xfd; Glossary v&#xe0; Translation Memory c&#x1ee7;a m&#xec;nh">
<edge COLOR="#990000" WIDTH="1"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1517228986768" ID="ID_695877794" MODIFIED="1522771223699" POSITION="right" TEXT="Chuy&#x1ec3;n &#x111;&#x1ed5;i d&#x1eef; li&#x1ec7;u">
<edge COLOR="#0033ff" WIDTH="4"/>
<node COLOR="#00b439" CREATED="1517229002856" ID="ID_582414427" MODIFIED="1522771223700" TEXT="Bilingual DOCX">
<edge COLOR="#00b439" WIDTH="2"/>
<arrowlink COLOR="#b0b0b0" DESTINATION="ID_1373759701" ENDARROW="Default" ENDINCLINATION="712;0;" ID="Arrow_ID_1833599197" STARTARROW="None" STARTINCLINATION="712;0;"/>
<linktarget COLOR="#b0b0b0" DESTINATION="ID_582414427" ENDARROW="Default" ENDINCLINATION="67;0;" ID="Arrow_ID_1433723454" SOURCE="ID_387339119" STARTARROW="None" STARTINCLINATION="67;0;"/>
<linktarget COLOR="#b0b0b0" DESTINATION="ID_582414427" ENDARROW="Default" ENDINCLINATION="76;0;" ID="Arrow_ID_1860720065" SOURCE="ID_212991424" STARTARROW="None" STARTINCLINATION="76;0;"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1517229580386" ID="ID_1139296440" MODIFIED="1522771223701" POSITION="left" TEXT="Quy tr&#xec;nh">
<edge COLOR="#0033ff" WIDTH="4"/>
<node COLOR="#00b439" CREATED="1517229591995" ID="ID_1757322560" MODIFIED="1522939973635" TEXT="File g&#x1ed1;c PDF -&gt; segment -&gt; t&#x1ea1;o file bilingual DOCX -&gt; d&#x1ecb;ch/hi&#x1ec7;u &#x111;&#xed;nh -&gt; merge, import into CAT -&gt; output file bilingual DOCX &#x111;&#xe3; d&#x1ecb;ch">
<edge COLOR="#00b439" WIDTH="2"/>
</node>
<node COLOR="#00b439" CREATED="1517229761612" ID="ID_325940275" MODIFIED="1522939951904" TEXT="Segment: Cafetran (convert PDF th&#xe0;nh DOCX r&#x1ed3;i segment), ho&#x1eb7;c Okapi Rainbow">
<edge COLOR="#00b439" WIDTH="2"/>
<icon BUILTIN="full-1"/>
</node>
<node COLOR="#00b439" CREATED="1517230275460" ID="ID_128456385" MODIFIED="1522940135132" TEXT="File Bilingual DOCX: Cafetran, ho&#x1eb7;c Okapi Rainbow, s&#x1eed;a ch&#xfa;t v&#x1ec1; &#x111;&#x1ecb;nh d&#x1ea1;ng b&#x1eb1;ng Word">
<edge COLOR="#00b439" WIDTH="2"/>
<icon BUILTIN="full-2"/>
</node>
<node COLOR="#00b439" CREATED="1517232426084" ID="ID_1239748662" MODIFIED="1522771223707" TEXT="Chia s&#x1ebb; Glossary v&#xe0; Translation Memory: trong team project OmegaT">
<edge COLOR="#00b439" WIDTH="2"/>
<icon BUILTIN="full-3"/>
</node>
<node COLOR="#00b439" CREATED="1517232052040" ID="ID_1812626816" MODIFIED="1522771223711" TEXT="D&#x1ecb;ch DOCX: d&#xf9;ng OmegaT v&#xe0; Gitlab (gitlab.com ho&#x1eb7;c Gitlab tr&#xea;n server ri&#xea;ng) cho team project">
<edge COLOR="#00b439" WIDTH="2"/>
<icon BUILTIN="full-4"/>
</node>
<node COLOR="#00b439" CREATED="1517232540004" ID="ID_1394095917" MODIFIED="1522940215835" TEXT="Merge: MS Word ch&#xe9;p n&#x1ed9;i dung d&#x1ecb;ch v&#xe0;o file Bilingual DOCX, d&#xf9;ng Heartsome TMX Editor &#x111;&#x1ec3; chuy&#x1ec3;n th&#xe0;nh file TMX, cho v&#xe0;o l&#x1ea1;i project OmegaT">
<edge COLOR="#00b439" WIDTH="2"/>
<icon BUILTIN="full-5"/>
</node>
<node COLOR="#00b439" CREATED="1517234142902" ID="ID_5914111" MODIFIED="1522940244490" TEXT="Output file bilingual DOCX &#x111;&#xe3; d&#x1ecb;ch xong b&#x1eb1;ng OmegaT">
<edge COLOR="#00b439" WIDTH="2"/>
<icon BUILTIN="full-6"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1517233180456" ID="ID_875620856" MODIFIED="1522771223713" POSITION="left" TEXT="C&#xe1;c h&#x1b0;&#x1edb;ng d&#x1eab;n c&#x1ea7;n thi&#x1ebf;t">
<edge COLOR="#0033ff" WIDTH="4"/>
<node COLOR="#00b439" CREATED="1517233190225" ID="ID_1138184436" MODIFIED="1522771223713" TEXT="Cho th&#xe0;nh vi&#xea;n d&#x1ecb;ch">
<edge COLOR="#00b439" WIDTH="2"/>
<node COLOR="#990000" CREATED="1517233207672" ID="ID_811409622" MODIFIED="1522771223714" TEXT="D&#xf9;ng Word g&#xf5; v&#xe0;o file Bilingual DOCX">
<edge COLOR="#990000" WIDTH="1"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1517233514170" ID="ID_1980953074" MODIFIED="1522771223714" TEXT="Cho th&#xe0;nh vi&#xea;n d&#x1ecb;ch b&#x1eb1;ng CAT OmegaT">
<edge COLOR="#00b439" WIDTH="2"/>
<node COLOR="#990000" CREATED="1517233200736" ID="ID_992269547" MODIFIED="1522771223714" TEXT="Giao di&#x1ec7;n ph&#x1ea7;n m&#x1ec1;m OmegaT, c&#xe1;ch t&#x1ea3;i project c&#x1ee7;a nh&#xf3;m">
<edge COLOR="#990000" WIDTH="1"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1517233701595" ID="ID_1659784283" MODIFIED="1522771223715" TEXT="Cho th&#xe0;nh vi&#xea;n d&#x1ecb;ch b&#x1eb1;ng CAT ri&#xea;ng">
<edge COLOR="#00b439" WIDTH="2"/>
<node COLOR="#990000" CREATED="1517233731395" ID="ID_1500165098" MODIFIED="1522771223715" TEXT="Gi&#x1ed1;ng nh&#x1b0; ng&#x1b0;&#x1edd;i d&#x1ecb;ch b&#x1eb1;ng tay: g&#x1eed;i file Bilingual DOCX">
<edge COLOR="#990000" WIDTH="1"/>
</node>
<node COLOR="#990000" CREATED="1517233749192" ID="ID_219383974" MODIFIED="1522771223716" TEXT="T&#x1ef1; x&#x1eed; l&#xfd; Glossary v&#xe0; Translation Memory v&#x1edb;i CAT m&#xe0; m&#xec;nh d&#xf9;ng">
<edge COLOR="#990000" WIDTH="1"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1517233271696" ID="ID_806374774" MODIFIED="1522771223716" TEXT="Cho tr&#x1b0;&#x1edf;ng nh&#xf3;m d&#x1ecb;ch">
<edge COLOR="#00b439" WIDTH="2"/>
<node COLOR="#990000" CREATED="1517233283368" ID="ID_668746456" MODIFIED="1522771223716" TEXT="Quy tr&#xec;nh t&#x1ed5; ch&#x1ee9;c project d&#x1ecb;ch nh&#xf3;m">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1520268861129" ID="ID_1426997514" MODIFIED="1522771220062" TEXT="xong">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#990000" CREATED="1517233295816" ID="ID_968703054" MODIFIED="1522771223717" TEXT="C&#xe1;ch qu&#x1ea3;n l&#xfd; d&#x1eef; li&#x1ec7;u Glossary">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1520268895973" ID="ID_1053981175" MODIFIED="1522771220063" TEXT="xong (&#x111;&#x1b0;a l&#xea;n Gitlab)">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#990000" CREATED="1517233327353" ID="ID_1350154294" MODIFIED="1522771223718" TEXT="C&#xe1;ch qu&#x1ea3;n l&#xfd; d&#x1eef; li&#x1ec7;u Translation Memory">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1522771500195" ID="ID_232050423" MODIFIED="1522771536188" TEXT="(d&#xf9;ng Heartsome TMX Editor)">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#990000" CREATED="1517233334426" ID="ID_546516273" MODIFIED="1522771223718" TEXT="C&#xe1;ch segmenting v&#xe0; export file Bilingual DOCX v&#x1edb;i Cafetran">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1520268922513" ID="ID_1691739668" MODIFIED="1522771220065" TEXT="xong">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#990000" CREATED="1522940287024" ID="ID_1880776684" MODIFIED="1522940333501" TEXT="C&#xe1;ch segmenting v&#xe0; export file Bilingual RTF v&#x1edb;i Okapi Rainbow">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1522940316940" ID="ID_1321074937" MODIFIED="1522940333502" TEXT="xong">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#990000" CREATED="1517233359929" ID="ID_1555818075" MODIFIED="1522771223719" TEXT="C&#xe1;ch t&#x1ea1;o ra team project v&#x1edb;i OmegaT v&#xe0; &#x111;&#x1b0;a l&#xea;n Gitlab">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1520268876313" ID="ID_265042349" MODIFIED="1522771220066" TEXT="xong">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#990000" CREATED="1517233883704" ID="ID_379603948" MODIFIED="1522771223719" TEXT="C&#xe1;ch qu&#x1ea3;n l&#xfd; th&#xe0;nh vi&#xea;n nh&#xf3;m d&#x1ecb;ch trong Gitlab">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1522771564076" ID="ID_1213750220" MODIFIED="1522771604312" TEXT="(thao t&#xe1;c tr&#xea;n Gitlab)">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#990000" CREATED="1517233485010" ID="ID_850170809" MODIFIED="1522771223719" TEXT="C&#xe1;ch import file Bilingual DOCX v&#xe0; output file d&#x1ecb;ch cu&#x1ed1;i v&#x1edb;i Cafetran">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1520268935265" ID="ID_752996" MODIFIED="1522771220068" TEXT="xong">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1517229502307" ID="ID_1031079765" MODIFIED="1522771223720" POSITION="right" TEXT="Ph&#x1ea7;n m&#x1ec1;m">
<edge COLOR="#0033ff" WIDTH="4"/>
<node COLOR="#00b439" CREATED="1517229506409" ID="ID_144930818" MODIFIED="1522771223720" TEXT="OmegaT">
<edge COLOR="#00b439" WIDTH="2"/>
<linktarget COLOR="#b0b0b0" DESTINATION="ID_144930818" ENDARROW="Default" ENDINCLINATION="772;0;" ID="Arrow_ID_1029679696" SOURCE="ID_212991424" STARTARROW="None" STARTINCLINATION="772;0;"/>
<node COLOR="#990000" CREATED="1517233059836" ID="ID_1122333266" MODIFIED="1522771223724" TEXT="Tr&#x1b0;&#x1edf;ng nh&#xf3;m: d&#xf9;ng &#x111;&#x1ec3; t&#x1ea1;o project, &#x111;&#x1b0;a v&#xe0;o Glossary v&#xe0; Translation Memory d&#xf9;ng chung, t&#x1ea3;i l&#xea;n kho trung t&#xe2;m tr&#xea;n m&#x1ea1;ng">
<edge COLOR="#990000" WIDTH="1"/>
</node>
<node COLOR="#990000" CREATED="1517233098415" ID="ID_1495361063" MODIFIED="1522771223729" TEXT="Ng&#x1b0;&#x1edd;i d&#x1ecb;ch: ch&#x1ec9; c&#x1ea7;n t&#x1ea3;i project t&#x1eeb; kho trung t&#xe2;m v&#x1ec1; m&#xe1;y &#x111;&#x1ec3; d&#x1ecb;ch (ph&#x1ea7;n m&#x1ec1;m t&#x1ef1; &#x111;&#x1ed9;ng k&#x1ebf;t n&#x1ed1;i d&#x1eef; li&#x1ec7;u v&#x1edb;i kho tr&#xea;n m&#x1ea1;ng)">
<edge COLOR="#990000" WIDTH="1"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1517229510098" ID="ID_1373759701" MODIFIED="1522937391400" TEXT="Cafetran ho&#x1eb7;c Okapi Framework">
<edge COLOR="#00b439" WIDTH="2"/>
<linktarget COLOR="#b0b0b0" DESTINATION="ID_1373759701" ENDARROW="Default" ENDINCLINATION="712;0;" ID="Arrow_ID_1833599197" SOURCE="ID_582414427" STARTARROW="None" STARTINCLINATION="712;0;"/>
<node COLOR="#990000" CREATED="1517233024473" ID="ID_1424994142" MODIFIED="1522771223731" TEXT="Ch&#x1ec9; c&#xf3; tr&#x1b0;&#x1edf;ng nh&#xf3;m c&#x1ea7;n d&#xf9;ng">
<edge COLOR="#990000" WIDTH="1"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1517234196297" ID="ID_1642719600" MODIFIED="1522771223731" POSITION="right" TEXT="M&#xf4; h&#xec;nh chia s&#x1ebb; d&#x1eef; li&#x1ec7;u">
<edge COLOR="#0033ff" WIDTH="4"/>
<node COLOR="#00b439" CREATED="1517234204642" ID="ID_483238677" MODIFIED="1522771223732" TEXT="D&#x1eef; li&#x1ec7;u l&#x1b0;u &#x1edf; kho trung t&#xe2;m, v&#xe0; tr&#xea;n PC t&#x1eeb;ng th&#xe0;nh vi&#xea;n d&#xf9;ng CAT">
<edge COLOR="#00b439" WIDTH="2"/>
</node>
<node COLOR="#00b439" CREATED="1517234221091" ID="ID_1403585323" MODIFIED="1522771223732" TEXT="Tr&#x1b0;&#x1edf;ng nh&#xf3;m d&#x1ecb;ch t&#x1ed5; ch&#x1ee9;c d&#x1eef; li&#x1ec7;u tham kh&#x1ea3;o (Glossary, Translation Memory, Dictionary)">
<edge COLOR="#00b439" WIDTH="2"/>
</node>
<node COLOR="#00b439" CREATED="1517234233657" ID="ID_1160286887" MODIFIED="1522771223732" TEXT="Th&#xe0;nh vi&#xea;n d&#x1ecb;ch c&#xe1;c file theo ph&#xe2;n c&#xf4;ng, tr&#xe1;nh c&#xf9;ng l&#xe0;m tr&#xea;n 1 file">
<edge COLOR="#00b439" WIDTH="2"/>
</node>
<node COLOR="#00b439" CREATED="1517234363111" ID="ID_1872987208" MODIFIED="1522771223733" TEXT="Tr&#x1b0;&#x1edf;ng nh&#xf3;m d&#x1ecb;ch qu&#x1ea3;n l&#xfd; d&#x1eef; li&#x1ec7;u &#x111;&#xe3; d&#x1ecb;ch xong (trao &#x111;&#x1ed5;i d&#x1eef; li&#x1ec7;u v&#x1edb;i nh&#xf3;m kh&#xe1;c)">
<edge COLOR="#00b439" WIDTH="2"/>
</node>
<node COLOR="#00b439" CREATED="1522771202429" ID="ID_1952567687" MODIFIED="1522771223774">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="mohinh-team-omegat-submodules.png" />
  </body>
</html></richcontent>
<edge COLOR="#00b439" WIDTH="2"/>
</node>
</node>
</node>
</map>
